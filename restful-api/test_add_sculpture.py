import json, requests, base64
from model import db, sculptures


def base64_photo():
    API = 'https://source.unsplash.com/random/700x400'

    res = requests.get(API, allow_redirects=True)
    print("========")
    print(res.url)
    print("========")
    # enc = base64.b64encode(res.content)
    # enc = str(enc.decode("utf-8"))

    return str(res.url)


all_sculpture = json.loads(open('test_data_sculpture.json').read())
all_sculpture = all_sculpture['sculpture']

for data in all_sculpture:
    artist_id = data['artist_id']
    name = data['name']
    description = data['description']
    photo = base64_photo()

    new_artist = sculptures(artist_id, name, photo, description)

    db.session.add(new_artist)
    db.session.commit()

    print(name + " added")
