from model import artist, paintings, sculptures


def check_login_credentials(email, password):
    demo_artist = artist.query.filter_by(email=email).first()
    if demo_artist is not None:
        if demo_artist.password == password:
            return True
        else:
            return False
    else:
        return False


def get_id_from_email(email):
    demo_artist = artist.query.filter_by(email=email).first()
    if demo_artist is not None:
        return demo_artist.id
    else:
        return None


def get_painting_from_id(painting_id):
    demo_paint = paintings.query.filter_by(id=painting_id).first()
    if demo_paint is not None:
        return demo_paint
    else:
        return None

def get_sculpture_from_id(sculpture_id):
    demo_sculpture = sculptures.query.filter_by(id=sculpture_id).first()
    if demo_sculpture is not None:
        return demo_sculpture
    else:
        return None