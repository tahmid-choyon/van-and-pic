from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from datetime import datetime
from sqlalchemy.dialects.mysql import LONGTEXT

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://root:letsplay@localhost:3306/artworkdb'
app.config['SECRET_KEY'] = 'restful_api_secret_key'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)


class admin(db.Model):
    id = db.Column('id', db.Integer, primary_key=True, autoincrement=True)
    email = db.Column('email', db.String(100), unique=True)
    username = db.Column('username', db.String(100), unique=True)
    password = db.Column('password', db.String(100))

    def __init__(self, email, username, password):
        self.email = email
        self.username = username
        self.password = password


class artist(db.Model):
    id = db.Column('id', db.Integer, primary_key=True, autoincrement=True)
    email = db.Column('email', db.String(100), unique=True)
    name = db.Column('name', db.String(100))
    password = db.Column('password', db.String(100))
    phone = db.Column('phone', db.String(500))
    gender = db.Column('gender', db.String(25))
    profile_photo = db.Column('profile_photo', db.String(1000))
    painted = db.relationship('paintings', backref='painter', lazy='dynamic')
    sculpted = db.relationship('sculptures', backref='sculpted', lazy='dynamic')

    def __init__(self, email, name, password, phone, gender, profile_photo=None):
        self.email = email
        self.name = name
        self.password = password
        self.phone = phone
        self.gender = gender
        self.profile_photo = profile_photo

    def json_dump(self):
        return {
            "id": self.id,
            "email": self.email,
            "name": self.name,
            "profile_photo": self.profile_photo
        }


class paintings(db.Model):
    id = db.Column('id', db.Integer, primary_key=True, autoincrement=True)
    artist_id = db.Column(db.Integer, db.ForeignKey('artist.id'))
    name = db.Column('name', db.String(200))
    description = db.Column('description', db.Text)
    photo = db.Column('photo', db.Text)
    upload_date = db.Column(db.DateTime)

    def __init__(self, artist_id, name, photo, description=None):
        self.artist_id = artist_id
        self.name = name
        self.photo = photo
        self.description = description
        self.upload_date = datetime.now()


class sculptures(db.Model):
    id = db.Column('id', db.Integer, primary_key=True, autoincrement=True)
    artist_id = db.Column(db.Integer, db.ForeignKey('artist.id'))
    name = db.Column('name', db.String(200))
    description = db.Column('description', db.Text)
    photo = db.Column('photo', db.Text)
    upload_date = db.Column(db.DateTime)

    def __init__(self, artist_id, name, photo, description=None):
        self.artist_id = artist_id
        self.name = name
        self.photo = photo
        self.description = description
        self.upload_date = datetime.now()
