# api
`Python3` and `Flask` powered **RESTful api** for artwork gallery management

## Technologies used
* Python 3
* Flask
* Flask SQLAlchemy
* Flask RESTless

## Demo `email` and `password` to test **Login** [`/login/`] function
| Email | Password |
| - | - |
| jenny.ward@example.com          | marcello  |
| jackson.fuller@example.com      | giovanna  |
| joe.garrett@example.com         | train     |
| hanna.freeman@example.com       | brandi    |
| paul.torres@example.com         | mariposa  |
| zoe.mills@example.com           | pppppp    |
| ted.richards@example.com        | kittens   |
| curtis.watkins@example.com      | ball      |
| kristin.knight@example.com      | atlantic  |
| mandy.oliver@example.com        | capital   |
| mia.freeman@example.com         | sylveste  |
| floyd.davies@example.com        | butterfl  |
| tyler.pearson@example.com       | season    |
| brittany.williamson@example.com | wrestler  |
| jenny.morales@example.com       | allegro   |
| ella.tucker@example.com         | chuai     |
| salvador.jensen@example.com     | houston   |
| becky.kuhn@example.com          | elizabeth |
| nicky.knight@example.com        | george1   |
| francis.cruz@example.com        | asdf12    |
| vanessa.williams@example.com    | linda1    |
| debbie.walker@example.com       | roman     |
| samuel.mason@example.com        | estrella  |
| cathy.ortiz@example.com         | lovely    |
| gerald.gregory@example.com      | faggot    |
| gene.gordon@example.com         | formula   |
| judith.davidson@example.com     | dang      |
| brian.gutierrez@example.com     | treefrog  |
| warren.kim@example.com          | blow      |
| evan.lawson@example.com         | lucky1    |
