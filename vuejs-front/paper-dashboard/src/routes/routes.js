import DashboardLayout from '../components/Dashboard/Layout/DashboardLayout.vue'
// GeneralViews
import NotFound from '../components/GeneralViews/NotFoundPage.vue'

// Admin pages
import UserProfile from 'src/components/Dashboard/Views/UserProfile.vue'
import Icons from 'src/components/Dashboard/Views/Icons.vue'
import Register from 'src/components/Dashboard/Views/Register.vue'
import Login from 'src/components/Dashboard/Views/Login.vue'
import ListShow from 'src/components/Dashboard/Views/List.vue'
import ListShowUser from 'src/components/Dashboard/Views/UserList.vue'
import Add from 'src/components/Dashboard/Views/AddPhoto.vue'
import Add2 from 'src/components/Dashboard/Views/AddSculpt.vue'
import Users from 'src/components/Dashboard/Views/ListUsers.vue'

const routes = [
    {
        path: '/',
        component: DashboardLayout,
        redirect: '/app/login'
    },
    {
        path: '/app',
        component: DashboardLayout,
        redirect: '/app/profile',
        children: [

            {
                path: 'profile',
                name: 'profile',
                component: UserProfile
            },
            {
                path: 'profile/:id',
                name: 'profileid',
                component: UserProfile
            },

            {
                path: 'add_painting',
                name: 'add painting',
                component: Add
            },
            {
                path: 'add_sculpture',
                name: 'add sculpture',
                component: Add2
            },
            {
                path: 'icons',
                name: 'icons',
                component: Icons
            },


            {
                path: 'paintings/:id',
                name: 'paintings',
                component: ListShow
            },
            {
                path: 'mypaintings/:id',
                name: 'mypaintings',
                component: ListShowUser
            },
            {
                path: 'sculptures/:id',
                name: 'sculptures',
                component: ListShow
            },
            {
                path: 'mysculptures/:id',
                name: 'mysculptures',
                component: ListShowUser
            },
            {
                path: 'login',
                name: 'login',
                component: Login
            },
            {
                path: 'register',
                name: 'register',
                component: Register
            },
            {path: 'users/:id', name: 'user_list', component: Users}

        ]
    },
    {path: '*', component: NotFound}
]

/**
 * Asynchronously load view (Webpack Lazy loading compatible)
 * The specified component must be inside the Views folder
 * @param  {string} name  the filename (basename) of the view to load.
 function view(name) {
   var res= require('../components/Dashboard/Views/' + name + '.vue');
   return res;
};**/

export default routes
