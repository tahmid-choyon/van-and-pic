import Sidebar from './SideBar.vue'

const SidebarStore = {
    showSidebar: true,
    sidebarLinks: [

        { name: 'User Profile', icon: 'ti-user', path: '/app/profile'},
        {name:'Login',icon:'ti-lock',path:'/app/login'},
        {name:'Register',icon:'ti-star',path:'/app/register'},
        {name:'All Paintings',icon:'ti-paint-bucket',path:'/app/paintings/1'},
        {name:'All Sculptures',icon:'ti-hummer',path:'/app/sculptures/1'},
        {name:'My Paintings',icon:'ti-paint-bucket',path:'/app/mypaintings/1'},
        {name:'My Sculptures',icon:'ti-hummer',path:'/app/mysculptures/1'},
    ],
    userLinks: [

        { name: 'User Profile', icon: 'ti-user', path: '/app/profile'},
        {name:'All Paintings',icon:'ti-paint-bucket',path:'/app/paintings/1'},
        {name:'All Sculptures',icon:'ti-hummer',path:'/app/sculptures/1'},
        {name:'My Paintings',icon:'ti-paint-bucket',path:'/app/mypaintings/1'},
        {name:'My Sculptures',icon:'ti-hummer',path:'/app/mysculptures/1'},
    ],
    guestLinks: [
        {name:'Login',icon:'ti-lock',path:'/app/login'},
        {name:'Register',icon:'ti-star',path:'/app/register'},
        {name:'All Paintings',icon:'ti-paint-bucket',path:'/app/paintings/1'},
        {name:'All Sculptures',icon:'ti-hummer',path:'/app/sculptures/1'},
    ],
    adminLinks: [
        {name:'All Users',icon:'ti-paint-bucket',path:'/app/users/1'},
        {name:'All Paintings',icon:'ti-paint-bucket',path:'/app/paintings/1'},
        {name:'All Sculptures',icon:'ti-hummer',path:'/app/sculptures/1'}
    ],
    displaySidebar (value) {
        this.showSidebar = value
    }
}


const SidebarPlugin = {

    install (Vue) {
        Vue.mixin({
            data () {
                return {
                    sidebarStore: SidebarStore
                }
            }
        })

        Object.defineProperty(Vue.prototype, '$sidebar', {
            get () {
                return this.$root.sidebarStore
            }
        })
        Vue.component('side-bar', Sidebar)
    }
}

export default SidebarPlugin
