import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export const store = new Vuex.Store({
    state: {

        token: localStorage.getItem('token')
    },
    getters: {

    },
    mutations: {
        login(state, payload){

            state.token = payload.token;
        },
        addClasses(state, payload){

        },
        logout(state){

            state.token = '';
            localStorage.clear();
        }
    }
});
